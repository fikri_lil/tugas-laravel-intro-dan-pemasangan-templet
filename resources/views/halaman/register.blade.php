@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
    <label >First Name:</label><br><br>
    <input type="text"name="nama"><br><br>
    <label >Last Name:</label><br><br>
    <input type="text" name="nama"><br><br>
    <label >Gender:</label><br><br>
    <input type="radio"name="jk">Male <br>
    <input type="radio"name="jk">Female <br><br>
    <label >Nationality</label><br><br>
    <select name="negara" >
        <option value="1">Indonesia</option>
        <option value="2">Amerika</option>
        <option value="3">Inggris</option>
    </select><br><br>
    <label>Language Spoken</label><br><br>
    <input type="checkbox" name="Skill">Bahasa Indonesia <br>
    <input type="checkbox" name="Skill">English <br>
    <input type="checkbox" name="Skill">Other <br><br>
    <label>Bio</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">
    </form>
    @endsection