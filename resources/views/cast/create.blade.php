@extends('layout.master')

@section('judul')
Tambah Pemain
@endsection

@section('content')

<form action = '/cast' method='POST'>
  @csrf
  <div class="mb-3">
    <label class="form-label">Nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <div class="mb-3">
    <label class="form-label">Umur</label>
    <input type="type" name="umur" class="form-control">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <div class="mb-3">
    <label >Bio</label>
    <textarea name="bio" class="form-control"></textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection