<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'indexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@selamatdatang');

Route::get('/data-table' ,function(){
    return view('halaman.table.data-table');
});

// CRUD cast
// Create
 //route menuju form create
 Route::get('/cast/create', 'castController@create'); //Route Menambahkan film baru
 Route::post('/cast', 'castController@store'); //Route untuk menyimpan data ke database

 //Read
 Route::get('/cast', 'castController@index'); //Route List Cast
 Route::get('/cast/{cast_id}', 'castController@show'); //Route detail Cast
 
 //Upadte
 Route::get('/cast/{cast_id}/edit', 'castController@edit'); //Route Menuju Form Edit
 Route::put('/cast/{cast_id}', 'castController@update'); //Route Untuk update data di database

 //Delete
 Route::delete('/cast/{cast_id}', 'castController@destroy'); //Route untuk menghapus data di database 